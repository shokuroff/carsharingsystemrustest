//
//  String.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/22/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation

extension String {

    enum ErrorMessages {
        static let noViewDecoderImplementation = "No implementation to call current method."
        static let unexpectedButton = "This button mustn't be implemented."
    }

    enum Keys {
        static let googleMapsKey = "AIzaSyDjVUi6Bq-E2eOgv30ojqb4W3fNQXCLd44"
    }

    enum ImageNames {
        static let DirectionOnMap = "DirectionButtonIcon"
        static let PositionOnMap = "PositionButtonIcon"
        static let ReloadOnMap = "ReloadButtonIcon"
        static let ScanOnMap = "ScanButtonIcon"
        static let ParkingMapMarker = "ParkingMarker"
        static let ScooterMapMarker = "ScooterMarker"
        static let CloseNavigationItem = "CloseNavigationItemIcon"
        static let BackNavigationItem = "BackNavigationItemIcon"
        static let ScooterInsideCell = "ScooterIconInsideCell"
        static let ScooterCellAccessory = "ScooterCellAccessoryIcon"
        static let Bicycle = "Bicycle"
    }

    enum LoadingTexts {
        static let loadingTitle = "Загрузка данных.."
        static let updatingTitle = "Обновление данных.."
        static let loadingParkingDetauls = "Загрузка парковки.."
        static let loadingScooters = "Загружаются прокатные скутеры.."
    }

    enum RegionIdentifiers {
        static let scootersRegionIdentifier = "ScootersRegionIdentifier"
    }

    enum FontNames {
        static let MontserrateBold = "Montserrat-Bold"
        static let MontserrateRegular = "Montserrat-Regular"
    }

    enum CellIdentifiers {
        static let scooter = "ScooterTableViewCellIdentifier"
    }

    enum UserNotificationContents {
        static let rentTitle = "Счастливого пути!"
        static let bookTitle = "Успешная бронь!"
    }

    enum UserNotificationThreadIdentifiers {
        static let rent = "LocalNotificationRentThreadIdentifier"
        static let book = "LocalNotificaiotnBookThreadIdentifier"
    }

}
