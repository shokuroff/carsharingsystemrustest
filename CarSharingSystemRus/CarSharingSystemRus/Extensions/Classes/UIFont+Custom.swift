//
//  UIFont+Custom.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/24/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit.UIFont

extension UIFont {

    // MARK: – Weight

    enum CustomWeight {
        case bold
        case regular
    }

    // MARK: – Montserrate

    static func custom(
        ofSize size: CGFloat = 17,
        withWeight weight: CustomWeight = .regular
    ) -> UIFont? {
        switch weight {
        case .regular:
            return UIFont(name: String.FontNames.MontserrateRegular, size: size)
        case .bold:
            return UIFont(name: String.FontNames.MontserrateBold, size: size)
        }
    }
}
