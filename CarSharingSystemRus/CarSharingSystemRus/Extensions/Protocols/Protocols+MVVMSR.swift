//
//  Protocols+MVVMSR.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation

protocol NavigationBarDelegate: class {
    func didTouchCloseButton()
    func didTouchBackButton()
}

protocol ViewControllerWithCallbacks: class {
    func didGetBadErrorWithTitle(_ title: String, andMessage rawMessage: String?)
    func didGetEmptyData()
    func didGetNewData()
}

protocol ViewModelWithCallbacks: class {
    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?)
    func didGetEmptyData()
    func didGetNewData()
}

protocol RouterWithCallbacks: class {
    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?)
    func didLoadFromNetworkData(_ data: Any)
}
