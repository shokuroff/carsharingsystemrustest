//
//  SectionWithTwoButtons.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/22/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class SectionWithTwoButtons: UIView, CustomizableView {

    // MARK: – Appearance

    enum Appearance {
        static let horizontalLineBackground = UIColor(
            red: 0.77,
            green: 0.77,
            blue: 0.77,
            alpha: 1
        )
        static let backgroundColor = UIColor.Interface.defaultBackground
        static let reloadIcon = UIImage(named: String.ImageNames.ReloadOnMap)
        static let positionIcon = UIImage(named: String.ImageNames.PositionOnMap)
        static let sectionSize = CGSize(width: 50, height: 105)
        static let shadowColor = UIColor.Interface.defaultText.cgColor
        static let shadowOpacity: Float = 0.25
        static let shadowOffset = CGSize(width: 4, height: 20)
        static let shadowRadius: CGFloat = 20
        static let shadowScale = true
        static let radius: CGFloat = 12
        static let buttonSize = CGSize(width: 50, height: 50)
        static let horizontalLineSize = CGSize(width: 37, height: 1)
        static let horizontalLineOffset = UIEdgeInsets(
            top: 0,
            left: 6,
            bottom: 0,
            right: 6
        )
    }

    // MARK: – Properties

    private lazy var reloadButton: ReloadButton = {
        let frame = CGRect(origin: .zero, size: Appearance.buttonSize)
        let button = ReloadButton(frame: frame)
        button.widthAnchor
            .constraint(equalToConstant: Appearance.buttonSize.width)
            .isActive = true
        button.heightAnchor
            .constraint(equalToConstant: Appearance.buttonSize.height)
            .isActive = true
        return button
    }()

    private lazy var horizontalLine: UIView = {
        let frame = CGRect(origin: .zero, size: Appearance.horizontalLineSize)
        let line = UIView(frame: frame)
        line.heightAnchor
            .constraint(equalToConstant: Appearance.horizontalLineSize.height)
            .isActive = true
        return line
    }()

    private lazy var positionButton: PositionButton = {
        let frame = CGRect(origin: .zero, size: Appearance.buttonSize)
        let button = PositionButton(frame: frame)
        button.widthAnchor
            .constraint(equalToConstant: Appearance.buttonSize.width)
            .isActive = true
        button.heightAnchor
            .constraint(equalToConstant: Appearance.buttonSize.height)
            .isActive = true
        return button
    }()

    // MARK: – Initialization

    required init(coder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        self.setup()
        self.addSubviews()
        self.setupSubviews()
        self.makeConstraints()
    }

    // MARK: – Public Methods

    final func set(_ delegate: MapsViewDelegate) {
        self.reloadButton.set(delegate)
        self.positionButton.set(delegate)
    }

    // MARK: – Private Methods

    private func setup() {
        self.setupBackground()
        self.applyCornerRadius()
        self.applyShadow()
    }

    private func setupBackground() {
        self.backgroundColor = Appearance.backgroundColor
    }

    private func applyCornerRadius() {
        self.layer.cornerRadius = Appearance.radius
    }

    private func applyShadow() {
        self.applyShadow(
            withColor: Appearance.shadowColor,
            opacity: Appearance.shadowOpacity,
            offset: Appearance.shadowOffset,
            radius: Appearance.shadowRadius,
            scale: Appearance.shadowScale
        )
    }

    // MARK: – CustomizableView

    func addSubviews() {
        self.addSubview(self.reloadButton)
        self.addSubview(self.horizontalLine)
        self.addSubview(self.positionButton)
    }

    func setupSubviews() {
        self.horizontalLine.backgroundColor = Appearance.horizontalLineBackground
    }

    func makeConstraints() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.reloadButton.snp.makeConstraints { make in
            make.top.right.left.equalToSuperview()
        }
        self.horizontalLine.snp.makeConstraints { make in
            make.top.equalTo(self.reloadButton.snp.bottom).offset(1)
            make.leading.equalToSuperview().offset(Appearance.horizontalLineOffset.left)
            make.trailing.equalToSuperview().offset(-Appearance.horizontalLineOffset.right)
        }
        self.positionButton.snp.makeConstraints { make in
            make.top.equalTo(self.horizontalLine.snp.bottom).offset(1)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }

}
