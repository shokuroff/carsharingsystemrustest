//
//  PositionButton.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/22/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class PositionButton: UIButton {

    // MARK: – Appearance

    private enum Appearance {
        static let icon = UIImage(named: String.ImageNames.PositionOnMap)
    }

    // MARK: – Private Properties

    private weak var delegate: MapsViewDelegate?

    // MARK: – Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        self.setup()
    }

    // MARK: – Public Methods

    final func set(_ delegate: MapsViewDelegate) {
        self.delegate = delegate
    }

    // MARK: – Private Methods

    @objc private func action(_ sender: UIButton) {
        self.delegate?.didTouchPositionButton()
    }

    private func setup() {
        self.setImage(Appearance.icon, for: .normal)
        self.addTarget(
            self,
            action: #selector(self.action(_:)),
            for: .touchUpInside
        )
    }

}
