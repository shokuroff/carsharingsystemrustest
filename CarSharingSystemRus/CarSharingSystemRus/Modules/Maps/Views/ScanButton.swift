//
//  ScanButton.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/22/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class ScanButton: UIButton {

    // MARK: – Appearance

    private enum Appearance {
        static let backgroundColor = UIColor.Interface.main
        static let icon = UIImage(named: String.ImageNames.ScanOnMap)
        static let shadowColor = UIColor.Interface.main.cgColor
        static let shadowOpacity: Float = 0.4
        static let shadowOffset = CGSize(width: 5, height: 10)
        static let shadowRadius: CGFloat = 10
        static let shadowScale = true
        static let radius: CGFloat = 12
    }

    // MARK: – Private Properties

    private weak var delegate: MapsViewDelegate?

    // MARK: – Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        self.setup()
    }

    // MARK: – Public Methods

    final func set(_ delegate: MapsViewDelegate) {
        self.delegate = delegate
    }

    // MARK: – Private Methods

    @objc private func action(_ sender: UIButton) {
        self.delegate?.didTouchScanButton()
    }

    private func setup() {
        self.setImage(Appearance.icon, for: .normal)
        self.backgroundColor = Appearance.backgroundColor
        self.layer.cornerRadius = Appearance.radius
        self.applyShadow(
            withColor: Appearance.shadowColor,
            opacity: Appearance.shadowOpacity,
            offset: Appearance.shadowOffset,
            radius: Appearance.shadowRadius,
            scale: Appearance.shadowScale
        )
        self.addTarget(
            self,
            action: #selector(self.action(_:)),
            for: .touchUpInside
        )
    }

}
