// 
//  MapsRouter.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation

protocol MapsRouterWithCallbacks: RouterWithCallbacks {
    func didGetScooters(_ scooters: [Scooter], for parking: ParkingPlace)
}

final class MapsRouter {

    // MARK: – Location

    typealias Location = (latitude: Double, longtitude: Double)

    // MARK: – Properties

    final weak private var viewModel: MapsViewModelWithCallbacks!
    final private var model: MapsModel
    final private let maxZoomToSearchParkings: Float = 17
    final private var serviceWorking = false

    // MARK: – Object Life Cycle

    init() {
        self.model = MapsModel()
    }

    convenience init(withViewModel viewModel: MapsViewModelWithCallbacks) {
        self.init()
        self.viewModel = viewModel
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
    }

    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }

    // MARK: – Public Accessors

    final func requestData(withZoom zoomFromMap: Float, inLocation location: Location) {
        let needsToFindScooters = zoomFromMap > self.maxZoomToSearchParkings
        guard !self.serviceWorking else {
            return // service is working currently..
        }
        let api = MapsService(withRouter: self)
        switch needsToFindScooters {
        case true:
            if let place = self.model.needsToLoadScootersBasedOnCurrentPosition(location) {
                self.serviceWorking = true
                api.requestScooters(for: place)
            } else {
                return
            }
        case false:
            self.serviceWorking = true
            api.requestParkings()
        }
    }

    final func loadData() -> [ParkingPlace] {
        return self.model.parkings
    }
    // swiftlint:disable identifier_name
    final func parkingPlaceWithIdentifier(_ id: Int) -> ParkingPlace? {
        return self.model.parkingPlaceWithIdentifier(id)
    }
    // swiftline:enable identifier_name
}

// MARK: – Network Callbacks

extension MapsRouter: MapsRouterWithCallbacks {

    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        self.serviceWorking = false
        self.viewModel.didGetNetworkRequestErrorWithTitle(title, andMessage: rawMessage)
    }

    func didLoadFromNetworkData(_ data: Any) {
        self.serviceWorking = false
        switch data {
        case is [ParkingPlace]:
            let parkings = data as? [ParkingPlace] ?? []
            self.model.saveData(parkings, callback: { success in
                if success {
                    self.viewModel.didGetNewData()
                } else {
                    self.viewModel.didGetEmptyData()
                }
            })
        default:
            self.viewModel.didGetEmptyData()
        }
    }

    func didGetScooters(_ scooters: [Scooter], for parking: ParkingPlace) {
        self.serviceWorking = false
        self.model.saveScooters(scooters, for: parking, callback: { success in
            if success {
                self.viewModel.didLoadScooters(scooters)
            } else {
                self.viewModel.didGetEmptyData()
            }
        })
    }
}
