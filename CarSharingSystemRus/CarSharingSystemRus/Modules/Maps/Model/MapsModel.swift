// 
//  MapsModel.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation
import CoreLocation

final class MapsModel {

    // MARK: – Location

    typealias Location = (latitude: Double, longtitude: Double)

    // MARK: – Completion Handler

    typealias CompletionHandler = ((Bool) -> Void)

    // MARK: – Properties

    private var data: [ParkingPlace] = []
    private let radiusRange: Double = 300 // in meters
    private var needsToShowScooterMarkers = false

    // MARK: – Object Life Cycle

    init() {
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
    }

    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }

    // MARK: – Getters

    final var dataCount: Int {
        return self.data.count
    }

    final var parkings: [ParkingPlace] {
        return self.data
    }

    // MARK: – Public Accessors

    final func saveData(_ data: [ParkingPlace], callback: @escaping CompletionHandler) {
        guard !data.isEmpty else {
            self.data.removeAll()
            callback(false)
            return
        }
        self.data = data
        callback(true)
    }

    final func saveScooters(
        _ scooters: [Scooter],
        for parkingPlace: ParkingPlace,
        callback: @escaping CompletionHandler
    ) {
        parkingPlace.reset(scooters)
        callback(true)
    }

    final func needsToLoadScootersBasedOnCurrentPosition(_ location: Location) -> ParkingPlace? {
        let center = CLLocationCoordinate2DMake(
            location.latitude,
            location.longtitude
        )
        let area = CLCircularRegion(
            center: center,
            radius: self.radiusRange,
            identifier: String.RegionIdentifiers.scootersRegionIdentifier
        )
        let results = self.data.filter { place in
            area.contains(place.coordinates)
        }
        return results.count == 1 ? results.first : nil
    }
    // swiftlint:disable identifier_name
    final func parkingPlaceWithIdentifier(_ id: Int) -> ParkingPlace? {
        let parkings = self.data.filter { $0.identifier == id }
        return parkings.count == 1 ? parkings.first! : nil
    }
    // swiftline:enable identifier_name
}
