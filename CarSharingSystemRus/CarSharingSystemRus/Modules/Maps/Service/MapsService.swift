// 
//  MapsService.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation

final class MapsService {

    // MARK: – Location and Address

    typealias Address = ParkingPlace.Address
    typealias Location = ParkingPlace.Location

    // MARK: – Properties

    weak private var router: MapsRouterWithCallbacks!

    // MARK: – Object Life Cycle

    convenience init(withRouter router: MapsRouterWithCallbacks) {
        self.init()
        self.router = router
    }

    // MARK: – Network requests

    final func requestParkings() {
        DispatchQueue.main.async {
            self.router.didLoadFromNetworkData(self.generateFakeRentPoints())
        }
    }

    final func requestScooters(for parkingPlace: ParkingPlace) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            let scooters = self.generateScootersForParkingPlace(parkingPlace)
            self.router.didGetScooters(scooters, for: parkingPlace)
        })
    }

    // MARK: – Private Methods

    private func generateFakeRentPoints() -> [ParkingPlace] {
        return [
            ParkingPlace.generateTestFirstPlace(),
            ParkingPlace.generateTestSecondPlace()
        ]
    }

    private func generateScootersForParkingPlace(_ place: ParkingPlace) -> [Scooter] {
        switch place.identifier {
        case 1:
            return Scooter.generateForFirstPlace()
        case 2:
            return Scooter.generateForSecondPlace()
        default:
            return []
        }
    }

}
