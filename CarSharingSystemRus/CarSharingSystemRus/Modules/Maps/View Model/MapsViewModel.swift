// 
//  MapsViewModel.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation
import GoogleMaps

protocol MapsViewControllerWithCallbacks: ViewControllerWithCallbacks {
    func didLoadScooterMarkers(_ markers: [GMSMarker])
}

protocol MapsViewModelWithCallbacks: ViewModelWithCallbacks {
    func didLoadScooters(_ scooters: [Scooter])
}

final class MapsViewModel {

	// MARK: - Properties

	weak private var viewController: MapsViewControllerWithCallbacks!
    private var router: MapsRouter!

	// MARK: - Object Life Cycle

	convenience init(withViewController viewController: MapsViewControllerWithCallbacks) {
		self.init()
		self.viewController = viewController
        #if DEBUG
		print("\n[\(self) init]\n")
        #endif
        self.router = MapsRouter(withViewModel: self)
	}

	deinit {
        #if DEBUG
		print("\n[\(self) deinit]\n")
        #endif
	}

    // MARK: – Public Accessors

    final func loadData(withZoom zoom: Float, inLocation location: CLLocation) {
        let currentLocation = (location.coordinate.latitude, location.coordinate.longitude)
        self.router.requestData(withZoom: zoom, inLocation: currentLocation)
    }

    final func requestToPresentMarkersOnMap() -> [GMSMarker] {
        let parkings = self.router.loadData()
        var markers: [GMSMarker] = []
        parkings.forEach { parkingPlace in
            let marker = GMSMarker(position: CLLocationCoordinate2DMake(
                parkingPlace.location.latitude,
                parkingPlace.location.longtitude
            ))
            marker.icon = UIImage(named: String.ImageNames.ParkingMapMarker)
            marker.userData = parkingPlace.identifier
            markers.append(marker)
        }
        return markers
    }
    // swiftlint:disable identifier_name
    final func parkingPlaceWithIdentifier(_ id: Int) -> ParkingPlace? {
        return self.router.parkingPlaceWithIdentifier(id)
    }
    // swiftline:enable identifier_name
}

// MARK: – Callbacks

extension MapsViewModel: MapsViewModelWithCallbacks {

    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        self.viewController.didGetBadErrorWithTitle(title, andMessage: rawMessage)
    }

    func didGetNewData() {
        self.viewController.didGetNewData()
    }

    func didGetEmptyData() {
        self.viewController.didGetEmptyData()
    }

    func didLoadScooters(_ scooters: [Scooter]) {
        let markers = scooters.map {
            GMSMarker(position: $0.coordinates)
        }
        markers.forEach {
            $0.icon = UIImage(named: String.ImageNames.ScooterMapMarker)
        }
        self.viewController.didLoadScooterMarkers(markers)
    }
}
