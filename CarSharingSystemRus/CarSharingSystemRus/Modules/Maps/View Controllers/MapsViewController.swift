//
//  MapsViewController.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/22/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit
import GoogleMaps

class MapsViewController: ResponsiveViewController {

    // MARK: – Appearance

    enum Appearance {
        static let defaultZoomValue: Float = 15.0
        static let north: CLLocationDirection = 0.0
    }

    // MARK: – Properties

    private let locationManager = CLLocationManager()

    private lazy var mapsView: MapsView = {
        let view = MapsView()
        view.set(self)
        view.setupMapDelegate(self)
        return view
    }()

    private var initialLaunch = true
    private var viewModel: MapsViewModel!
    private var currentLocation: CLLocation = .init()
    private var currentZoom: Float = 15.0

    // MARK: – View Life Cycle

    override func loadView() {
        super.loadView()
        self.view = self.mapsView
        self.setupLocationManager()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLoadingDescription(String.LoadingTexts.loadingTitle)
        self.prepareResponsiveViewWithContentContainer(UIView())
        self.viewModel = MapsViewModel(withViewController: self)
        self.viewModel.loadData(withZoom: Appearance.defaultZoomValue, inLocation: self.currentLocation)
    }

    // MARK: – Private methods

    private func setupLocationManager() {
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
    }

}

// MARK: – Location Manager Delegate

extension MapsViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse, manager == self.locationManager else {
            return
        }
        self.locationManager.startUpdatingLocation()
        self.viewState = .loading
        self.mapsView.isMyLocationEnabled = true
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last, manager == self.locationManager else {
            return
        }
        self.currentLocation = location
        let camera = GMSCameraPosition(
            target: location.coordinate,
            zoom: Appearance.defaultZoomValue,
            bearing: 0,
            viewingAngle: 0
        )
        let launchInitially = self.initialLaunch
        switch launchInitially {
        case true:
            self.initialLaunch = false
            self.mapsView.set(camera)
        case false:
            self.mapsView.animate(toCamera: camera, completion: {})
        }
        self.locationManager.stopUpdatingLocation()
        self.viewState = .normal
    }

}

// MARK: – Google Maps Delegate

extension MapsViewController: GMSMapViewDelegate {

    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        guard let visibleMarker = self.mapsView.visibleMarkers.first else {
            return
        }
        let locationWhereToLoad = CLLocation(
            latitude: visibleMarker.position.latitude,
            longitude: visibleMarker.position.longitude
        )
        let oldZoomValue = self.currentZoom
        if oldZoomValue != self.mapsView.currentZoom {
            self.currentZoom = self.mapsView.currentZoom
            self.viewModel.loadData(
                withZoom: self.currentZoom,
                inLocation: locationWhereToLoad
            )
        }
    }

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let position = GMSCameraPosition(
            latitude: marker.position.latitude,
            longitude: marker.position.longitude,
            zoom: self.currentZoom
        )
        self.mapsView.animate(toCamera: position, completion: {
            guard let parkingPlaceIdentifier = marker.userData as? Int  else {
                return // no needs to open parking details modally.
            }
            guard let parkingPlaceToPresent = self.viewModel.parkingPlaceWithIdentifier(
                parkingPlaceIdentifier
            ) else {
                return // no data in model with current selected marker!
            }
            let assembler = PlaceAssembler(withPlaceToPresent: parkingPlaceToPresent)
            let viewControllerToPresent = assembler.makeModule()
            self.present(viewControllerToPresent, animated: true)
        })
        return true
    }

}

// MARK: – Maps View Delegate

extension MapsViewController: MapsViewDelegate {

    func didTouchDirectionButton() {
        self.mapsView.set(Appearance.north)
    }

    func didTouchScanButton() {
        #if DEBUG
        print("\n[\(self) didTouchScanButton]\n")
        #endif
    }

    func didTouchReloadButton() {
        #if DEBUG
        print("\n[\(self) didTouchReloadButton]\n")
        #endif
    }

    func didTouchPositionButton() {
        self.locationManager.startUpdatingLocation()
    }

}

// MARK: – View Controller with Callbacks

extension MapsViewController: MapsViewControllerWithCallbacks {

    func didGetBadErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        self.setErrorDescription(title: title, message: rawMessage)
        self.viewState = .error
    }

    func didGetNewData() {
        self.viewState = .normal
        let markers = self.viewModel.requestToPresentMarkersOnMap()
        self.mapsView.set(markers)
    }

    func didGetEmptyData() {
        self.viewState = .empty
    }

    func didLoadScooterMarkers(_ markers: [GMSMarker]) {
        self.mapsView.set(markers)
    }
}
