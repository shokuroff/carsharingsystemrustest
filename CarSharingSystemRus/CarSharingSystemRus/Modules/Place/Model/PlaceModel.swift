// 
//  PlaceModel.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit.UIImage

final class PlaceModel {

    // MARK: – Private Properties

    private var data: ParkingPlace?

    // MARK: – Object Life Cycle

    init() {
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
    }

    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }

    // MARK: – Getters

    final var dataCount: Int {
        return 0
    }

    final var sectionsCount: Int {
        return 1
    }

    final var currentPlaceIdentifier: Int {
        return self.data?.identifier ?? 0
    }

    final var currentTitleOfPlace: String {
        return self.data?.title ?? ""
    }

    final var currentPhotoOfPlace: UIImage? {
        return self.data?.photo
    }

    final var currentAddressOfPlace: ParkingPlace.Address? {
        return self.data?.address
    }

    final var scootersCount: Int {
        return self.data?.scootersCount ?? 0
    }

    // MARK: – Public Accessors

    final func savePlace(_ place: ParkingPlace) {
        self.data = place
    }

    final func saveScooters(_ scooters: [Scooter], completion: @escaping () -> Void) {
        if let place = self.data {
            place.append(scooters)
        }
        completion()
    }

    final func scooter(atIndex index: Int) -> Scooter? {
        return self.data?.scooter(atIndex: index)
    }
}
