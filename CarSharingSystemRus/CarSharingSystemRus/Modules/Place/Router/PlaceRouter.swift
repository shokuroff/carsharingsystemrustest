// 
//  PlaceRouter.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit.UIImage

final class PlaceRouter {

    // MARK: – Properties

    final weak private var viewModel: ViewModelWithCallbacks!
    final private var model: PlaceModel

    // MARK: – Object Life Cycle

    init() {
        self.model = PlaceModel()
    }

    convenience init(withViewModel viewModel: ViewModelWithCallbacks) {
        self.init()
        self.viewModel = viewModel
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
    }

    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }

    // MARK: – Getters

    final var sectionsCount: Int {
        return self.model.sectionsCount
    }

    final var dataTitle: String {
        return self.model.currentTitleOfPlace
    }

    final var photoPlace: UIImage? {
        return self.model.currentPhotoOfPlace
    }

    final var address: ParkingPlace.Address? {
        return self.model.currentAddressOfPlace
    }

    final var rowsInTable: Int {
        return self.model.scootersCount
    }

    // MARK: – Public Accessors

    final func getItemAtIndex(_ index: Int) -> Scooter? {
        return self.model.scooter(atIndex: index)
    }

    final func requestData() {
        let service = PlaceService(withRouter: self)
        service.requestScootersForParkingPlace(self.model.currentPlaceIdentifier)
    }

    final func saveData(_ data: ParkingPlace) {
        self.model.savePlace(data)
    }

}

// MARK: – Network Callbacks

extension PlaceRouter: RouterWithCallbacks {

    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        self.viewModel.didGetNetworkRequestErrorWithTitle(title, andMessage: rawMessage)
    }

    func didLoadFromNetworkData(_ data: Any) {
        guard let scooters = data as? [Scooter] else {
            self.viewModel.didGetNetworkRequestErrorWithTitle(
                "Пришли некорректные данные",
                andMessage: nil
            )
            return
        }
        self.model.saveScooters(scooters, completion: {
            if scooters.isEmpty {
                self.viewModel.didGetEmptyData()
            } else {
                self.viewModel.didGetNewData()
            }
        })
    }
}
