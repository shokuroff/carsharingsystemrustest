//
//  PlaceNavigationController.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class PlaceNavigationController: UINavigationController {

    override var shouldAutorotate: Bool {
        return false
    }

}
