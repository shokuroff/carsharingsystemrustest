//
//  ScooterCellAccessoryView.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class ScooterCellAccessoryView: UIView, CustomizableView {

    // MARK: – Appearance

    private enum Appearance {
        static let iconSize = CGSize(width: 13.68, height: 11.57)
        static let image = UIImage(
            named: String.ImageNames.ScooterCellAccessory
        )!
        static let contentMode = UIView.ContentMode.scaleAspectFit
    }

    // MARK: – UI

    private lazy var icon = UIImageView()

    // MARK: – Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        self.addSubviews()
        self.setupSubviews()
        self.makeConstraints()
    }

    // MARK: – CustomizableView

    func addSubviews() {
        self.addSubview(icon)
    }

    func setupSubviews() {
        self.icon.image = Appearance.image
        self.icon.contentMode = Appearance.contentMode
    }

    func makeConstraints() {
        self.icon.translatesAutoresizingMaskIntoConstraints = false
        self.icon.snp.makeConstraints { make in
            make.size.equalTo(Appearance.iconSize)
            make.center.equalToSuperview()
        }
    }
}
