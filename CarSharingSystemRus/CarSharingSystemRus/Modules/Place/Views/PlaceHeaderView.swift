//
//  PlaceHeaderView.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/24/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit
import SnapKit

class PlaceHeaderView: UIView, CustomizableView {

    // MARK: – View Model

    typealias ViewModel = (
        photo: UIImage?,
        address: String,
        totalText: String
    )

    private var viewModel: ViewModel

    // MARK: – NSAttributedString Keys

    typealias AttributedKey = NSAttributedString.Key

    // MARK: – Appearance

    private enum Appearance {
        // General:
        static let contentHorizontalMargin: CGFloat = 16
        static let marginBetweenLabelsAndPhoto: CGFloat = 16
        static let marginBetweenLabels: CGFloat = 5
        static let bottomMargin: CGFloat = 12
        // Labels:
        static let labelAlignment = NSTextAlignment.left
        static let labelFont = UIFont.custom(ofSize: 16, withWeight: .regular)!
        static let labelTextColor = UIColor.Interface.defaultText
        static let numberOfLinesInAddressLabel = 0 // no limits
        static let numberOfLinesInTotalLabel = 2
        // Photo:
        static let photoTopMargin: CGFloat = 20
        static let photoRadius: CGFloat = 14
        static let photoWidth: CGFloat = 288
        static let photoHeight: CGFloat = 150
        static let photoSize = CGSize(
            width: Appearance.photoWidth,
            height: Appearance.photoHeight
        )
        static let photoContentMode = ContentMode.scaleAspectFit
    }

    // MARK: – UI

    private lazy var photoImageView = UIImageView()
    private lazy var addressLabel = UILabel()
    private lazy var totalLabel = UILabel()

    // MARK: – Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    init(frame: CGRect = .zero, withViewModel viewModel: ViewModel) {
        self.viewModel = viewModel
        super.init(frame: frame)
        self.addSubviews()
        self.setupSubviews()
        self.makeConstraints()
    }

    // MARK: – Public Methods

    final func updateTotalText(byString string: String) {
        self.viewModel.totalText = string
        self.totalLabel.attributedText = self.generateAttributedString(
            byString: self.viewModel.totalText
        )
    }

    // MARK: – Private Methods

    private func setupPhotoView() {
        self.photoImageView.layer.cornerRadius = Appearance.photoRadius
        self.photoImageView.contentMode = Appearance.photoContentMode
        if let photo = self.viewModel.photo {
            self.photoImageView.image = photo
        } else {
            self.photoImageView.image = nil
            self.photoImageView.backgroundColor = .lightGray
        }
    }

    private func setupAddressLabel() {
        self.addressLabel.numberOfLines = Appearance.numberOfLinesInAddressLabel
        self.addressLabel.attributedText = self.generateAttributedString(
            byString: self.viewModel.address
        )
    }

    private func setupTotalLabel() {
        self.totalLabel.numberOfLines = Appearance.numberOfLinesInTotalLabel
        self.totalLabel.attributedText = self.generateAttributedString(
            byString: self.viewModel.totalText
        )
    }

    private func makeConstraintsForPhotoView() {
        self.photoImageView.translatesAutoresizingMaskIntoConstraints = false
        self.photoImageView.frame.size = Appearance.photoSize
        self.photoImageView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(Appearance.contentHorizontalMargin)
            make.right.equalToSuperview().offset(-Appearance.contentHorizontalMargin)
            let size = Appearance.photoSize
            let multiplier = size.height / size.width // <- ratio
            make.height.equalTo(self.photoImageView.snp.width).multipliedBy(multiplier)
            make.top.equalToSuperview().offset(Appearance.photoTopMargin)
        }
    }

    private func makeConstraintsForAddressLabel() {
        self.addressLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addressLabel.snp.makeConstraints { make in
            make.top
                .equalTo(self.photoImageView.snp.bottom)
                .offset(Appearance.marginBetweenLabelsAndPhoto)
            make.left.equalToSuperview().offset(Appearance.contentHorizontalMargin)
            make.right.equalToSuperview().offset(-Appearance.contentHorizontalMargin)
        }
    }

    private func makeConstraintsForTotalLabel() {
        self.totalLabel.translatesAutoresizingMaskIntoConstraints = false
        self.totalLabel.snp.makeConstraints { make in
            make.top
                .equalTo(self.addressLabel.snp.bottom)
                .offset(Appearance.marginBetweenLabels)
            make.left.equalToSuperview().offset(Appearance.contentHorizontalMargin)
            make.right.equalToSuperview().offset(-Appearance.contentHorizontalMargin)
            make.bottom.equalToSuperview().offset(-Appearance.bottomMargin)
        }
    }

    private func generateAttributedString(byString string: String) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = Appearance.labelAlignment
        let attributedString = NSAttributedString(
            string: string,
            attributes: [
                AttributedKey.font: Appearance.labelFont,
                AttributedKey.paragraphStyle: paragraphStyle
            ]
        )
        return attributedString
    }

    // MARK: – CustomizableView

    func addSubviews() {
        self.addSubview(self.photoImageView)
        self.addSubview(self.addressLabel)
        self.addSubview(self.totalLabel)
    }

    func setupSubviews() {
        self.setupPhotoView()
        self.setupAddressLabel()
        self.setupTotalLabel()
    }

    func makeConstraints() {
        self.makeConstraintsForPhotoView()
        self.makeConstraintsForAddressLabel()
        self.makeConstraintsForTotalLabel()
    }

}
