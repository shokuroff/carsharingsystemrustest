//
//  PlaceAssembler.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class PlaceAssembler: Assembler {

    // MARK: – Properties

    private let parkingPlaceToPresent: ParkingPlace?

    // MARK: – Initialization

    init(withPlaceToPresent parkingPlace: ParkingPlace?) {
        self.parkingPlaceToPresent = parkingPlace
    }

    // MARK: – Assembler

    func makeModule() -> UIViewController {
        let viewController = PlaceViewController()
        let navigationController = PlaceNavigationController(
            rootViewController: viewController
        )
        if let place = self.parkingPlaceToPresent {
            viewController.saveInModelParkingPlace(place)
        }
        navigationController.modalPresentationStyle = .overFullScreen
        return navigationController
    }
}
