// 
//  PlaceViewModel.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation

final class PlaceViewModel {

	// MARK: - Properties

	weak private var viewController: ViewControllerWithCallbacks!
    private var router: PlaceRouter!

    private let defaultAddressText = "Нет данных по адресу"
    private let defaultTotalText = "Техника не прогружена.."
    private let defaultCellTitle = "Ошибка"
    private let defaultCellSubtitle = "Заряд: 0%"

	// MARK: - Object Life Cycle

    init() {
        self.additionInitialization()
    }

	convenience init(withViewController viewController: ViewControllerWithCallbacks) {
		self.init()
		self.viewController = viewController
	}

    private func additionInitialization() {
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
        self.router = PlaceRouter(withViewModel: self)
    }

	deinit {
        #if DEBUG
		print("\n[\(self) deinit]\n")
        #endif
	}

	// MARK: - Getters

    final var sectionsCount: Int {
		return self.router.sectionsCount
	}

    final var rowsCount: Int {
		return self.router.rowsInTable
	}

    final var placeTitle: String {
        return self.router.dataTitle
    }

    final var totalStringInHeader: String {
        return self.generateFormattedTotalText()
    }

    final var dataForHeader: PlaceView.HeaderViewModel {
        let formattedAddress = self.generateFormattedAddressText(byAddress: self.router.address)
        let formattedTotal = self.totalStringInHeader
        return (self.router.photoPlace, formattedAddress, formattedTotal)
    }

    // MARK: – Public Accessors

    final func getCellDataForRow(_ row: Int) -> PlaceView.CellViewModel {
        let scooter = self.router.getItemAtIndex(row)
        let cellTitle = self.generateCellTitle(byScooter: scooter)
        let cellSubtitle = self.generateCellSubtitle(byScooter: scooter)
        return (cellTitle, cellSubtitle)
    }

    final func loadPossibleScooters() {
        self.router.requestData()
    }

    final func savePlace(_ place: ParkingPlace) {
        self.router.saveData(place)
    }

    final func setDelegate(_ viewController: ViewControllerWithCallbacks) {
        self.viewController = viewController
    }

    final func scooterToPresent(atRow index: Int) -> Scooter? {
        return self.router.getItemAtIndex(index)
    }

    // MARK: – Private Methods

    private func generateFormattedAddressText(byAddress rawAddress: ParkingPlace.Address?) -> String {
        guard let address = rawAddress else {
            return self.defaultAddressText
        }
        return "улица \(address.street), дом \(address.house)."
    }

    private func generateFormattedTotalText() -> String {
        let scootersCount = self.router.rowsInTable
        guard scootersCount > 0 else {
            return self.defaultTotalText
        }
        switch scootersCount {
        case 1:
            return "\(scootersCount) самокат"
        case 2...4:
            return "\(scootersCount) самоката"
        case 5...:
            return "\(scootersCount) самокатов"
        default:
            return self.defaultTotalText
        }
    }

    private func generateCellTitle(byScooter rawScooter: Scooter?) -> String {
        guard let scooter = rawScooter else {
            return self.defaultCellTitle
        }
        return "№ \(scooter.identifier)"
    }

    private func generateCellSubtitle(byScooter rawScooter: Scooter?) -> String {
        guard let scooter = rawScooter else {
            return self.defaultCellSubtitle
        }
        return "Заряд: \(scooter.batteryLevel)%"
    }

}

// MARK: – Callbacks

extension PlaceViewModel: ViewModelWithCallbacks {

    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        self.viewController.didGetBadErrorWithTitle(title, andMessage: rawMessage)
    }

    func didGetNewData() {
        self.viewController.didGetNewData()
    }

    func didGetEmptyData() {
        self.viewController.didGetEmptyData()
    }
}
