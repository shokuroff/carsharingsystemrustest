// 
//  PlaceService.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation

final class PlaceService {

    // MARK: – Properties

    weak private var router: RouterWithCallbacks!

    // MARK: – Object Life Cycle

    convenience init(withRouter router: RouterWithCallbacks) {
        self.init()
        self.router = router
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
    }

    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }

    // MARK: – Network requests

    final func requestScootersForParkingPlace(_ placeIdentifier: Int) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            switch placeIdentifier {
            case 1:
                self.router.didLoadFromNetworkData(Scooter.generateForFirstPlace())
            case 2:
                self.router.didLoadFromNetworkData(Scooter.generateForSecondPlace())
            default:
                self.router.didGetNetworkRequestErrorWithTitle("Нет данных", andMessage: nil)
            }
        })
    }

}
