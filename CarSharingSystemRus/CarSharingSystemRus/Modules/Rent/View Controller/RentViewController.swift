// 
//  RentViewController.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit
import GoogleMaps

final class RentViewController: ResponsiveViewController {

    // MARK: – Appearance

    typealias BarAppearance = NavigationController.Appearance

	// MARK: - Properties

	private var viewModel: RentViewModel
    private let locationManager = CLLocationManager()
    private let defaultZoomValue: Float = 15.0
    private var initialLaunch = true
    private var currentLocation: CLLocation = .init()

    // MARK: – UI

    private lazy var rentView: RentView = {
        let view = RentView(
            frame: UIScreen.main.bounds,
            withDataAboutDevice: self.viewModel.dataForInformationView
        )
        view.set(self)
        view.backgroundColor = .white
        return view
    }()

    // MARK: – Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.viewModel = RentViewModel()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

	// MARK: - View Life Cycle

    override func loadView() {
        super.loadView()
        self.view = self.rentView
        self.setupNavigationController()
        self.setupLocationManager()
    }

	override func viewDidLoad() {
		super.viewDidLoad()
		self.setLoadingDescription(String.LoadingTexts.loadingTitle)
        self.prepareResponsiveViewWithContentContainer(UIView())
	}

	deinit {
		#if DEBUG
		print("\n[\(self) deinit]\n")
		#endif
	}

    // MARK: – Public Methods

    final func saveInModelScooter(_ scooter: Scooter) {
        self.viewModel.saveScooter(scooter)
    }

    // MARK: – Private Methods

    private func setupLocationManager() {
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
    }

    private func setupNavigationController() {
        self.setupNavigationBackButton()
        self.setupNavigationTitle()
        self.setupNavigationBottomShadow()
    }

    private func setupNavigationBackButton() {
        let view = BackItemView()
        view.set(self)
        let item = UIBarButtonItem(customView: view)
        self.navigationItem.setLeftBarButton(item, animated: false)
    }

    private func setupNavigationTitle() {
        let title = self.viewModel.navigationTitleText
        self.viewState = .normal
        self.navigationItem.title = title
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: BarAppearance.titleFont
        ]
    }

    private func setupNavigationBottomShadow() {
        self.navigationController?.navigationBar.applyShadow(
            withColor: BarAppearance.shadowColor,
            opacity: BarAppearance.shadowOpacity,
            offset: BarAppearance.shadowOffset,
            radius: BarAppearance.shadowRadius,
            scale: BarAppearance.shadowScale
        )
    }

}

// MARK: – Navigation Bar Delegate

extension RentViewController: NavigationBarDelegate {

    func didTouchCloseButton() {
        fatalError(String.ErrorMessages.unexpectedButton)
    }

    func didTouchBackButton() {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

// MARK: – Rent View Delegate

extension RentViewController: RentViewDelegate {

    func didTouchLeftButtonInFooter() {
        self.viewModel.handleFooterLeftButton()
    }

    func didTouchRightButtonInFooter() {
        self.viewModel.handleFooterRightButton()
    }

}

// MARK: – Location Manager Delegate

extension RentViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse, manager == self.locationManager else {
            return
        }
        self.locationManager.startUpdatingLocation()
        self.viewState = .loading
        self.rentView.isMyLocationEnabled = true
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last, manager == self.locationManager else {
            return
        }
        self.currentLocation = location
        let camera = GMSCameraPosition(
            target: location.coordinate,
            zoom: self.defaultZoomValue,
            bearing: 0,
            viewingAngle: 0
        )
        let launchInitially = self.initialLaunch
        switch launchInitially {
        case true:
            self.initialLaunch = false
            self.rentView.set(camera)
        case false:
            self.rentView.animate(toCamera: camera, completion: {})
        }
        self.locationManager.stopUpdatingLocation()
        self.viewState = .normal
    }

}

// MARK: - Callbacks

extension RentViewController: ViewControllerWithCallbacks {

    func didGetBadErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        self.setErrorDescription(title: title, message: rawMessage)
        self.viewState = .error
    }

    func didGetNewData() {
        self.viewState = .normal
    }

    func didGetEmptyData() {
        self.viewState = .empty
    }

}
