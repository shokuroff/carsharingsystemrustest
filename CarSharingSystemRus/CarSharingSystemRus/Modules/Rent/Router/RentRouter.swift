// 
//  RentRouter.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation

final class RentRouter {

    // MARK: – Properties

    final weak private var viewModel: ViewModelWithCallbacks!
    final private var model: RentModel

    // MARK: – Object Life Cycle

    init() {
        self.model = RentModel()
    }

    convenience init(withViewModel viewModel: ViewModelWithCallbacks) {
        self.init()
        self.viewModel = viewModel
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
    }

    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }

    // MARK: – Getters

    final var title: String {
        return self.model.currentStreetName
    }

    final var pricePerMinute: Float {
        return self.model.devicePrice.perMinute
    }

    final var priceToUnlock: Float {
        return self.model.devicePrice.toUnlock
    }

    final var deviceIdentifier: Int {
        return self.model.deviceIdentifier
    }

    final var deviceName: String {
        return self.model.deviceName
    }

    final var batteryLevel: Int {
        return self.model.deviceBatteryLevel
    }

    // MARK: – Public Accessors

    final func saveData(_ data: Scooter) {
        self.model.save(data)
    }

    final func initiateRent() {
        guard let scooter = self.model.targetScooterToRentOrBook else {
            return
        }
        RentService(withRouter: self).rent(scooter)
    }

    final func initiateBooking() {
        guard let scooter = self.model.targetScooterToRentOrBook else {
            return
        }
        RentService(withRouter: self).book(scooter)
    }

}

// MARK: – Network Callbacks

extension RentRouter: RouterWithCallbacks {

    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        self.viewModel.didGetNetworkRequestErrorWithTitle(title, andMessage: rawMessage)
    }

    func didLoadFromNetworkData(_ data: Any) {
    }

}
