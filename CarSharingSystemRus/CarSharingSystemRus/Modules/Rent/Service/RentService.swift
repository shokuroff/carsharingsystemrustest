// 
//  RentService.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation
import UserNotifications

final class RentService {

    // MARK: – Notification Contents

    typealias Content = String.UserNotificationContents
    typealias ThreadIdentifiers = String.UserNotificationThreadIdentifiers

    // MARK: – Properties

    weak private var router: RouterWithCallbacks!

    private lazy var notificationCenter = UNUserNotificationCenter.current()

    // Server connection immitation:
    private let deadlineToRent = Date(timeIntervalSinceNow: 10)
    private let deadlineToBook = Date(timeIntervalSinceNow: 5)

    // MARK: – Object Life Cycle

    convenience init(withRouter router: RouterWithCallbacks) {
        self.init()
        self.router = router
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
    }

    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }

    // MARK: – Requests

    final func rent(_ scooter: Scooter) {
        let content = UNMutableNotificationContent()
        content.title = Content.rentTitle
        content.body = "Вы выбрали \(scooter.title)."
        content.sound = .default
        content.threadIdentifier = ThreadIdentifiers.rent
        let dateComponents = Calendar.current.dateComponents(
            [.year, .month, .day, .hour, .minute, .second],
            from: self.deadlineToRent
        )
        let trigger = UNCalendarNotificationTrigger(
            dateMatching: dateComponents,
            repeats: false
        )
        let notificationRequest = UNNotificationRequest(
            identifier: ThreadIdentifiers.rent,
            content: content,
            trigger: trigger
        )
        self.notificationCenter.add(notificationRequest)
    }

    final func book(_ scooter: Scooter) {
        let content = UNMutableNotificationContent()
        content.title = Content.bookTitle
        content.body = "Вы выбрали \(scooter.title)."
        content.sound = .default
        content.threadIdentifier = ThreadIdentifiers.book
        let dateComponents = Calendar.current.dateComponents(
            [.year, .month, .day, .hour, .minute, .second],
            from: self.deadlineToBook
        )
        let trigger = UNCalendarNotificationTrigger(
            dateMatching:
            dateComponents, repeats: false
        )
        let notificationRequest = UNNotificationRequest(
            identifier: ThreadIdentifiers.book,
            content: content,
            trigger: trigger
        )
        self.notificationCenter.add(notificationRequest)
    }

}
