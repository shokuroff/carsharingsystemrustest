//
//  RentFooterView.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class RentFooterView: UIView, CustomizableView {

    // MARK: – Appearance

    private enum Appearance {
        static let leftButtonTitle = "начать аренду"
        static let rightButtonTitle = "забронировать"
        static let leftButtonBackgroundColor = UIColor.Interface.main
        static let rightButtonBackgroundColor = UIColor.Interface.defaultBackground
        static let buttonTextFont = UIFont.custom(ofSize: 14, withWeight: .bold)!
        static let buttonTextAlignment = NSTextAlignment.center
        static let leftButtonTextColor = UIColor.Interface.defaultBackground
        static let rightButtonTextColor = UIColor.Interface.defaultText
        static let buttonMargins: UIEdgeInsets = .zero
        static let buttonHeight: CGFloat = 47
    }

    // MARK: – Button Type

    private enum Button {
        case left
        case right
    }

    // MARK: – Attributed Key

    typealias AttributedKey = NSAttributedString.Key

    // MARK: – Private Properties

    private weak var delegate: RentViewDelegate?

    // MARK: – UI

    private lazy var leftButton = UIButton()
    private lazy var rightButton = UIButton()

    // MARK: – Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubviews()
        self.setupSubviews()
        self.makeConstraints()
    }

    // MARK: – Public Methods

    final func set(_ delegate: RentViewDelegate) {
        self.delegate = delegate
    }

    // MARK: – Private Methods

    private func setupLeftButton() {
        self.leftButton.backgroundColor = Appearance.leftButtonBackgroundColor
        let attributedTitle = self.generateAttributedTitleForButton(
            .left,
            byString: Appearance.leftButtonTitle.uppercased()
        )
        self.leftButton.setAttributedTitle(attributedTitle, for: .normal)
        self.leftButton.addTarget(
            self,
            action: #selector(self.leftButtonAction(_:)),
            for: .touchUpInside
        )
    }

    private func setupRightButton() {
        self.rightButton.backgroundColor = Appearance.rightButtonBackgroundColor
        let attributedTitle = self.generateAttributedTitleForButton(
            .right,
            byString: Appearance.rightButtonTitle.uppercased()
        )
        self.rightButton.setAttributedTitle(attributedTitle, for: .normal)
        self.rightButton.addTarget(
            self,
            action: #selector(self.rightButtonAction(_:)),
            for: .touchUpInside
        )
    }

    private func makeConstraintsForLeftButton() {
        self.leftButton.translatesAutoresizingMaskIntoConstraints = false
        self.leftButton.snp.makeConstraints { make in
            make.top.left.bottom.equalToSuperview()
            make.right.equalTo(self.snp.centerX)
            make.height.equalTo(Appearance.buttonHeight)
        }
    }

    private func makeConstraintsForRightButton() {
        self.rightButton.translatesAutoresizingMaskIntoConstraints = false
        self.rightButton.snp.makeConstraints { make in
            make.top.right.bottom.equalToSuperview()
            make.left.equalTo(self.snp.centerX)
            make.height.equalTo(Appearance.buttonHeight)
        }
    }

    // MARK: – Button Actions

    @objc private func leftButtonAction(_ sender: UIButton) {
        self.delegate?.didTouchLeftButtonInFooter()
    }

    @objc private func rightButtonAction(_ sender: UIButton) {
        self.delegate?.didTouchRightButtonInFooter()
    }

    // MARK: – Helpers

    private func generateAttributedTitleForButton(
        _ button: Button,
        byString string: String
    ) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = Appearance.buttonTextAlignment
        let color = button == .left ? Appearance.leftButtonTextColor : Appearance.rightButtonTextColor
        return NSAttributedString(
            string: string,
            attributes: [
                AttributedKey.font: Appearance.buttonTextFont,
                AttributedKey.foregroundColor: color,
                AttributedKey.paragraphStyle: paragraphStyle
            ]
        )
    }

    // MARK: – CustomizableView

    func addSubviews() {
        self.addSubview(self.leftButton)
        self.addSubview(self.rightButton)
    }

    func setupSubviews() {
        self.setupLeftButton()
        self.setupRightButton()
    }

    func makeConstraints() {
        self.makeConstraintsForLeftButton()
        self.makeConstraintsForRightButton()
    }
}
