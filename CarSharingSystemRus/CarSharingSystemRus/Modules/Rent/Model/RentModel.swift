// 
//  RentModel.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation

final class RentModel {

    // MARK: – Private Properties

    private var data: Scooter?

    // MARK: – Object Life Cycle

    init() {
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
    }

    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }

    // MARK: – Getters

    final var targetScooterToRentOrBook: Scooter? {
        return self.data
    }

    final var currentStreetName: String {
        return self.data?.parkingAddress.street ?? ""
    }

    final var devicePrice: Scooter.Price {
        return self.data?.price ?? (0.0, 0.0)
    }

    final var deviceIdentifier: Int {
        return self.data?.identifier ?? 0
    }

    final var deviceName: String {
        return self.data?.title ?? ""
    }

    final var deviceBatteryLevel: Scooter.BatteryLevel {
        return self.data?.batteryLevel ?? 0
    }

    // MARK: – Public Accessors

    final func save(_ newScooter: Scooter) {
        self.data = newScooter
    }

}
