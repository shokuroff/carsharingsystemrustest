//
//  RentAssembler.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class RentAssembler: Assembler {

    // MARK: – Properties
    func makeModule() -> UIViewController {
        return RentViewController()
    }
}
