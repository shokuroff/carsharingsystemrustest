//
//  AppDelegate.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/22/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit
import GoogleMaps
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: – Properties

    var window: UIWindow?

    // MARK: – Notification Options

    private let notificationOptionsToAuthorize: UNAuthorizationOptions = [
        .sound, .badge, .alert
    ]

    private let notificationPresentationOptions: UNNotificationPresentationOptions = [
        .sound, .badge, .alert
    ]

    // MARK: – Application Life Cycle

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        self.setupGoogleMaps()
        self.setupRootView()
        self.accessPermissionForNotifications()
        return true
    }

    // MARK: – Private Methods

    private func setupGoogleMaps() {
        GMSServices.provideAPIKey(String.Keys.googleMapsKey)
    }

    private func setupRootView() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let mapsViewController = MapsAssembler().makeModule()
        self.window?.rootViewController = mapsViewController
        self.window?.makeKeyAndVisible()
    }

    private func accessPermissionForNotifications() {
        let notificationCenter = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = self.notificationOptionsToAuthorize
        notificationCenter.requestAuthorization(options: options, completionHandler: { (_, error) in
            if error != nil {
                print("\n[\(self) accessPermissionForNotifications]\n\(error!.localizedDescription)\n")
            }
        })
        notificationCenter.delegate = self
    }

}

// MARK: – User Notification Center Delegate

extension AppDelegate: UNUserNotificationCenterDelegate {

    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        completionHandler(self.notificationPresentationOptions)
    }

}
