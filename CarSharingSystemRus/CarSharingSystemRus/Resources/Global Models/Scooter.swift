//
//  Scooter.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation
import CoreLocation

class Scooter {

    static let defaultScooterName = "Электровелосипед Ducati City King"
    static let defaultRatePerMinute: Float = 2.5
    static let defaultPriceToUnlock: Float = 10.0

    typealias BatteryLevel = Int
    typealias Location = ParkingPlace.Location
    typealias Address = ParkingPlace.Address
    typealias Price = (perMinute: Float, toUnlock: Float)

    // MARK: – Properties

    let identifier: Int
    let title: String
    let batteryLevel: BatteryLevel
    let location: Location
    let parkingAddress: Address
    let price: Price

    // Initialization

    init(
        identifier: Int,
        title: String = Scooter.defaultScooterName,
        batteryLevel level: Int,
        location: Location,
        address: Address,
        price: Price = (
            Scooter.defaultRatePerMinute,
            Scooter.defaultPriceToUnlock
        )
    ) {
        self.identifier = identifier
        self.title = title
        self.batteryLevel = level > -1 && level < 101 ? level : 0
        self.location = location
        self.parkingAddress = address
        var correctPricePerMinute: Float = 0.0
        var correctPriceToUnlock: Float = 0.0
        if price.perMinute >= 0.0 {
            correctPricePerMinute = price.perMinute
        }
        if price.toUnlock >= 0.0 {
            correctPriceToUnlock = price.toUnlock
        }
        self.price = (correctPricePerMinute, correctPriceToUnlock)
    }

    // MARK: – Getters

    var coordinates: CLLocationCoordinate2D {
        return CLLocationCoordinate2DMake(
            self.location.latitude,
            self.location.longtitude
        )
    }

    // MARK: – Static Test Functionality

    class func generateForFirstPlace() -> [Scooter] {
        let firstLocation = Location(43.124575, 131.886818)
        let firstScooter = Scooter(
            identifier: 7543806943,
            batteryLevel: 57,
            location: firstLocation,
            address: ParkingPlace.generateTestAddressForFirstPlace()
        )
        let secondLocation = Location(43.124557, 131.886947)
        let secondScooter = Scooter(
            identifier: 7543506949,
            batteryLevel: 75,
            location: secondLocation,
            address: ParkingPlace.generateTestAddressForFirstPlace()
        )
        let thirdLocation = Location(43.124614, 131.886898)
        let thirdScooter = Scooter(
            identifier: 7203306953,
            batteryLevel: 100,
            location: thirdLocation,
            address: ParkingPlace.generateTestAddressForFirstPlace()
        )
        return [firstScooter, secondScooter, thirdScooter]
    }

    class func generateForSecondPlace() -> [Scooter] {
        let firstLocation = Location(43.115687, 131.885163)
        let firstScooter = Scooter(
            identifier: 7543806943,
            batteryLevel: 98,
            location: firstLocation,
            address: ParkingPlace.generateTestAddressForSecondPlace()
        )
        let secondLocation = Location(43.115701, 131.885027)
        let secondScooter = Scooter(
            identifier: 7543506949,
            batteryLevel: 100,
            location: secondLocation,
            address: ParkingPlace.generateTestAddressForSecondPlace()
        )
        let thirdLocation = Location(43.115759, 131.884997)
        let thirdScooter = Scooter(
            identifier: 7200006943,
            batteryLevel: 76,
            location: thirdLocation,
            address: ParkingPlace.generateTestAddressForSecondPlace()
        )
        return [firstScooter, secondScooter, thirdScooter]
    }

}
