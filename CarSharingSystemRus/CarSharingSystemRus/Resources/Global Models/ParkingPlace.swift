//
//  ParkingPlace.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit
import CoreLocation

class ParkingPlace: Equatable {

    // MARK: – Location

    typealias Location = (latitude: Double, longtitude: Double)

    // MARK: – Address

    struct Address {
        let street: String
        let house: String
        let additionalDescription: String?
    }

    // MARK: – Properties

    let identifier: Int
    let title: String
    let address: Address
    let location: Location
    let photo: UIImage?
    private var scooters: [Scooter]

    // MARK: – Initialization

    init(
        identifier: Int,
        title: String,
        address: Address,
        location: Location,
        photo: UIImage?,
        scooters: [Scooter] = []
    ) {
        self.identifier = identifier
        self.title = title
        self.address = address
        self.location = location
        self.photo = photo
        self.scooters = scooters
    }

    // MARK: – Getters

    var scootersCount: Int {
        return self.scooters.count
    }

    final func scooter(atIndex index: Int) -> Scooter? {
        guard index > -1 && index < self.scootersCount else {
            return nil
        }
        return self.scooters[index]
    }

    var coordinates: CLLocationCoordinate2D {
        return CLLocationCoordinate2DMake(
            self.location.latitude,
            self.location.longtitude
        )
    }

    // MARK: – Setters

    final func reset(_ scooters: [Scooter]) {
        self.scooters.removeAll()
        self.scooters.append(contentsOf: scooters)
    }

    final func append(_ scooters: [Scooter]) {
        self.scooters.append(contentsOf: scooters)
    }

    final func append(_ scooter: Scooter) {
        self.scooters.append(scooter)
    }

    // MARK: – Equatable

    static func == (lhs: ParkingPlace, rhs: ParkingPlace) -> Bool {
        return lhs.identifier == rhs.identifier
    }

    // MARK: – Test Static Methods

    class func generateTestAddressForFirstPlace() -> Address {
        return Address(
            street: "Алеутская",
            house: "45а",
            additionalDescription: nil
        )
    }

    class func generateTestAddressForSecondPlace() -> Address {
        return Address(
            street: "Светланская",
            house: "13",
            additionalDescription: nil
        )
    }

    class func generateTestFirstPlace() -> ParkingPlace {
        let location = Location(43.124607, 131.886896)
        return ParkingPlace(
            identifier: 1,
            title: "Парковка №1",
            address: ParkingPlace.generateTestAddressForFirstPlace(),
            location: location,
            photo: nil
        )
    }

    class func generateTestSecondPlace() -> ParkingPlace {
        let location = Location(43.115578, 131.885244)
        return ParkingPlace(
            identifier: 2,
            title: "Парковка №2",
            address: ParkingPlace.generateTestAddressForSecondPlace(),
            location: location,
            photo: nil
        )
    }

}
