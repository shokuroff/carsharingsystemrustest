# Personal test task

## Input

1. Экран карты с парковкой, которая распадается на транспортные маркеры с анимацией. Карта содержит текущее положение пользователя. (Есть видео-пример)
2. При клике на парковку открывается экран парковки с транспортом
3. При клике на транспорт в парковке строится маршрут от пользователя до маркера транспорта, открывается карточка транспорта.
4. При клике на транспорт на карте строится маршрут от пользователя до маркера транспорта, отрывается карточка транспорта.
5. Использовать карты Google.
Ключ для тестов можно зарегистрировать на себя.

There is [a design template link in Figma](https://www.figma.com/file/ByHXyQFs9AQGEatSCTXsNL/Test?node-id=0%3A1).

## Install, compile and run

1. `git clone https://gitlab.com/shokuroff/carsharingsystemrustest ~/Desktop/shokuroff`
2. `cd Desktop/shokuroff/CarSharingSystemRus`
3. `pod install`
4. `open CarSharingSystemRus.xcworkspace/` (*should be opened by Apple IDE called [Xcode](https://developer.apple.com/xcode/).*)
5. type `CMD + B` to build and `CMD + R`to run on selected device you need in.

**p.s.** Do not forget to put your own provisioning profile, certificate and Bundle ID in `CarSharingSystemRus/CarSharingSystemRus.xcodeproj` via Xcode, please.